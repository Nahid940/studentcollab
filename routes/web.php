<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/')->group(function (){
    Route::get('','FrontEndController@index')->name('index');
});

Route::prefix('learners')->group(function (){
//    Route::get('login','Auth\StudentLoginController@showLoginForm')->name('studentlogin');
//    Route::post('studentsubmitlogin','Auth\StudentLoginController@login')->name('studentsubmitlogin');
    Route::get('logout','Auth\LoginController@logout')->name('logout');

//    Route::get('register','Auth\StudentRegistrationController@shoRegistrationForm')->name('studentreg');
//    Route::post('doregister','Auth\StudentRegistrationController@register')->name('submitstdreg');
    Route::get('/',['uses'=>'Student\StudentController@index','roles'=>'student','as'=>'studentindex']);
    Route::get('/questions',['uses'=>'Student\StudentController@questions','as'=>'studentquestions'] );
    Route::post('/studentpostquestion',['uses'=>'Student\StudentQuestionController@studentpostquestion','roles'=>'student','as'=>'studentpostquestion']);
    Route::post('/postcomment',['uses'=>'Student\StudentQuestionController@postcomment','roles'=>['student','teacher'],'as'=>'postcomment']);
    Route::get('/eachquestion/{id}/{id1}',['uses'=>'Student\StudentQuestionController@eachquestion','roles'=>['student'],'as'=>'eachquestion']);

    Route::get('/group',['uses'=>'GroupController@index','roles'=>'student','as'=>'group']);
    Route::get('/group/{id}',['uses'=>'GroupController@viewgroup','roles'=>'student','as'=>'viewgroup']);
    Route::post('createGroup',['uses'=>'GroupController@createGroup','roles'=>'student','as'=>'createGroup']);
    Route::get('/deleteGroup/{id}',['uses'=>'GroupController@deleteGroup','roles'=>'student','as'=>'deleteGroup']);
    Route::post('postDiscussion',['uses'=>'DiscussionController@addDiscussion','roles'=>'student','as'=>'postDiscussion']);
    Route::get('/getAllDiscussion/{id}',['uses'=>'DiscussionController@getAllDiscussion','as'=>'getAllDiscussion','roles'=>['student','teacher']]);
    Route::post('/comment',['uses'=>'DiscussionController@comment','as'=>'comment','roles'=>['student','teacher']]);
});



Route::post('/mark',['uses'=>'QuestionPointController@mark','roles'=>['student','teacher'],'as'=>'mark']);
Route::post('/favourite',['uses'=>'QuestionPointController@favourite','roles'=>['student','teacher'],'as'=>'favourite']);
Route::post('/postreply',['uses'=>'ReplyController@postreply','roles'=>['student','teacher'],'as'=>'postreply']);
Route::prefix('experts')->group(function (){
//    Route::get('login','Auth\TeacherLoginController@showLoginForm')->name('teacherlogin');
//    Route::post('teachersubmitlogin','Auth\TeacherLoginController@login')->name('teachersubmitlogin');
//    Route::get('teacherlogout','Auth\TeacherLoginController@logout')->name('teacherlogout');
//    Route::get('register','Auth\TeacherRegistrationController@shoRegistrationForm')->name('teacherreg');
//    Route::post('doreggister','Auth\TeacherRegistrationController@register')->name('submittchrreg');
    Route::get('/',['uses'=>'Teacher\TeacherController@index','roles'=>'teacher','as'=>'teacherindex']);
    Route::post('/addBlog',['uses'=>'TeacherBlogController@addBlog','roles'=>'teacher','as'=>'addBlog']);
    Route::get('/blog/{id}/{id1}',['uses'=>'TeacherBlogController@eachblog','roles'=>'teacher','as'=>'eachblog']);
});


Route::get('/user/{id}',['uses'=>'UserController@users','as'=>'users','roles'=>['student','teacher']]);
Route::get('/addnew/{id}',['uses'=>'UserController@addnew','as'=>'addnew','roles'=>['student','teacher']]);
Route::get('/circle',['uses'=>'UserController@circle','as'=>'circle','roles'=>['student','teacher']]);
Route::post('/managecircle',['uses'=>'UserController@managecircle','as'=>'managecircle','roles'=>['student','teacher']]);
Route::get('/teachers-blogs',['uses'=>'TeacherBlogController@studentaccesstoblog','as'=>'studentaccesstoblog','roles'=>['student','teacher']]);
Route::get('/blog',['uses'=>'TeacherBlogController@index','as'=>'blog','roles'=>['student','teacher']]);
Route::post('/postcommentonblog',['uses'=>'TeacherBlogController@postcommentonblog','as'=>'postcommentonblog','roles'=>['student','teacher']]);
Route::get('/blogs',['uses'=>'TeacherBlogController@blogs','as'=>'blogs','roles'=>['student','teacher']]);
Route::get('/question/{id}',['uses'=>'Student\StudentQuestionController@ecachQuestion','as'=>'ecachQuestion','roles'=>['student','teacher']]);
Route::get('favouritequestions',['uses'=>'Student\StudentQuestionController@favouritequestions','as'=>'favouritequestions','roles'=>['student','teacher']]);
Route::resource('resources', 'ResourcesController');

Route::post('/circle',['uses'=>'UserController@addOrRemove','as'=>'addOrRemove','roles'=>['student','teacher']]);


Route::prefix('admin')->group(function (){
    Route::get('/',['uses'=>'Admin\AdminController@index','as'=>'adminindex','roles'=>'admin']);
    Route::get('/totalTopic',['uses'=>'Admin\AdminController@totalTopic','as'=>'totalTopic','roles'=>'admin']);
    Route::get('/pendingquestion',['uses'=>'Admin\QuestionsController@newquestions','as'=>'newquestions','roles'=>'admin']);
    Route::get('/approvequestion/{id}',['uses'=>'Admin\QuestionsController@approvequestion','as'=>'approvequestion','roles'=>'admin']);
    Route::get('/removequestion/{id}',['uses'=>'Admin\QuestionsController@removequestion','as'=>'removequestion','roles'=>'admin']);
    Route::get('/allusers',['uses'=>'Admin\UserController@allusers','as'=>'allusers','roles'=>'admin']);
    Route::get('/allusers/{id}',['uses'=>'Admin\UserController@removeUser','as'=>'removeUser','roles'=>'admin']);
    Route::get('/approve/{id}',['uses'=>'Admin\UserController@approve','as'=>'approve','roles'=>'admin']);
});

Route::get('/data','TestController@data');


