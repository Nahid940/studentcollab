@extends('layouts.master')

@section('content')


    <div class="container-fluid">
        <div class="row justify-content-lg-center">

            <div class="col-8">
                <h4>All the questions</h4>

                @if(($questions!=null))

                @foreach($questions->question as $d)

                    <div class="card">

                        <div class="card-header">
                            <span style="color: #1a8ff4;">Question asked by {{$d->user->name}} | {{$d->created_at->diffForHumans()}}</span>
                        </div>

                        <div class="card-body">
                            <p class="card-text">{{$d->question}}</p>

                            <div>
                                <b>Comments</b>
                                @foreach($d->comments as $c)
                                    <p style="background-color: #fffef9"><img src="{{asset('image/'.$c->user->image)}}" alt="User image" width="40px" style="border-radius: 20px"> <strong>{{$c->user->name}}  said</strong><br><span style="margin-left: 20px">{{$c->comment}}</span></p>

                                    <div style="margin-left: 40px;color: #7a7070">
                                        <p><i class="fas fa-reply"></i> Replies</p>
                                        @foreach($c->reply as $r)
                                            <p style="background-color: #f4f4f4"><strong>{{$r->user->name}} replied {{$r->created_at->diffForHumans()}} </strong> <br><span style="margin-left: 15px">{{$r->reply}}</span></p>
                                        @endforeach
                                        <a href="javascript:void(0)"  data-toggle="collapse" data-target="#demo{{$c->id}}">Reply</a>

                                        <div id="demo{{$c->id}}" class="collapse" >
                                            <form action="{{route('postreply')}}" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{$c->id}}" name="commentid">
                                                <p><input type="text" name="reply" ><button type="submit"><i class="fas fa-reply"></i></button></p>
                                            </form>
                                        </div>
                                    </div>
                                @endforeach




                            </div>


                            <a href=""  data-toggle="collapse" data-target="#collapseExample{{$d->id}}">Comment</a>
                            <div class="collapse" id="collapseExample{{$d->id}}">
                                <form action="{{route('postcomment')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="question_id" value="{{$d->id}}">
                                    <input type="hidden" name="author_id" value="{{$d->user_id}}">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="comment" name="comment" placeholder="Write your comment">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Comment</button>
                                </form>
                            </div>
                        </div>


                    </div>
                    &nbsp;
                @endforeach

                    @else
                    <div class="card">

                        <div class="card-header">
                           <h3> There is no questions that matches your topics.</h3>
                        </div>
                    </div>
                    @endif
            </div>
        </div>
    </div>

@endsection

