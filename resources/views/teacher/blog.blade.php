@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-lg-center">

        <div class="col-8">

            @if(session('blogPosted'))
                <div class="alert alert-success">
                    {{session('blogPosted')}}
                </div>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('teacher'))

                    <h4>Write blog</h4>
                    <div class="card">

                        <div class="card-header">

                        </div>

                        <div class="card-body">
                            <form id="submitPost" enctype="multipart/form-data" action="{{route('addBlog')}}" method="post">
                                {{ csrf_field() }}

                                <div class="row">
                                    <textarea id="mytextarea" name="texts"></textarea>
                                </div>


                                &nbsp;
                                <div class="row">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="validatedCustomFile" name="image">
                                        <label class="custom-file-label" for="validatedCustomFile">Choose files...</label>
                                    </div>
                                </div>

                                &nbsp;
                                <div class="row">

                                    <button class="btn btn-primary" type="submit">Post content</button>
                                </div>
                            </form>
                        </div>
                    </div>
            @endif
                    &nbsp;
        </div>
    </div>


    <div class="row justify-content-lg-center">

        <div class="col-8">

            <h4>All of your recent blogs</h4>
            @foreach($blog=\App\BlogPost::with('user')->orderBy('created_at','desc')->get() as $b)
            <div class="card">

                <div class="card-header">
                    @if(\Illuminate\Support\Facades\Auth::user()->id==$b->user_id)
                        Posted by you on {{$b->created_at->diffForHumans()}}
                    @else
                        Posted by {{$b->user->name}} {{$b->created_at->diffForHumans()}}
                    @endif
                </div>

                <div class="card-body">

                    <img src="{{asset('image/'.$b->image)}}" width="200px" alt="">
                        @php echo $b->blog
                        @endphp


                    <div>
                        @foreach($b->comment as $c)
                           <p> <img src="{{asset('image/'.$c->user->image)}}" alt="" width="40px" style="border-radius: 20px"> <strong>{{$c->user->name}}  said</strong><br> {{$c->comment}}  <small>{{$c->created_at->diffForHumans()}}</small></p>
                        @endforeach
                    </div>

                    <a href=""  data-toggle="collapse" data-target="#collapseExample{{$b->id}}">Comment</a>

                    <div class="collapse" id="collapseExample{{$b->id}}">
                        <form action="{{route('postcommentonblog')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="blog_id" value="{{$b->id}}">
                            <div class="form-group">
                                <input type="text" class="form-control" id="comment" name="comment" placeholder="Write your comment">
                            </div>
                            <button type="submit" class="btn btn-primary">Comment</button>
                        </form>
                    </div>
                </div>
            </div>
                &nbsp;
            @endforeach

        </div>
    </div>
</div>
    @endsection

@section('script')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>

    <script>
        $(document).ready(function () {
            tinymce.init({
                entity_encoding : "raw",
                selector:'textarea',
                element_format : 'html'
            });
        })
    </script>
@endsection