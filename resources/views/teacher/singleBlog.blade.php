@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-lg-center">

            <div class="col-8">

                <h4>All of your recent blogs</h4>

                    <div class="card">

                        <div class="card-header">
                            Posted by you on {{$b->created_at->diffForHumans()}}
                        </div>

                        <div class="card-body">
                            @php echo $b->blog
                            @endphp


                            <div>
                                @foreach($b->comment as $c)
                                     <p><img src="{{asset('image/'.$c->user->image)}}" alt="" width="40px" style="border-radius: 20px"> <strong>{{$c->user->name}} said</strong>  <br> {{$c->comment}}  <small>{{$c->created_at->diffForHumans()}}</small></p>
                                @endforeach
                            </div>

                            <a href=""  data-toggle="collapse" data-target="#collapseExample{{$b->id}}">Comment</a>

                            <div class="collapse" id="collapseExample{{$b->id}}">
                                <form action="{{route('postcommentonblog')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="blog_id" value="{{$b->id}}">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="comment" name="comment" placeholder="Write your comment">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Comment</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    &nbsp;

            </div>
        </div>
    </div>

@endsection