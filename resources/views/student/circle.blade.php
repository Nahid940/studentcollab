
{{--@foreach($data as $d)--}}
    {{--{{$d->user->name}}--}}
    {{--{{$d->user->id}}--}}
{{--@endforeach--}}
@extends('layouts.master')
@section('css')

    <style>
        .list-content{
            min-height:300px;
        }
        .list-content .list-group .title{
            background:#5bc0de;
            border:2px solid #DDDDDD;
            font-weight:bold;
            color:#FFFFFF;
        }
        .list-group-item img {
            height:80px;
            width:80px;
        }

        .jumbotron .btn {
            padding: 5px 5px !important;
            font-size: 12px !important;
        }
        .prj-name {
            color:#5bc0de;
        }
        .break{
            width:100%;
            margin:20px;
        }
        .name {
            color:#5bc0de;
        }

    </style>
@endsection

@section('content')
    <div class="jumbotron list-content" id="users">
        <ul class="list-group">
            <li href="#" class="list-group-item title">
                Your  circle
            </li>
            @foreach($data as $d)
                <li href="#" class="list-group-item text-left" style="float: left">
                    <img class="img-thumbnail" src="{{asset('image/'.$d->user->image)}}">
                    <label class="name">
                        <a href="{{url('user/'.$d->user->id)}}"> {{$d->user->name}} </a><br>
                    </label>
                    <label class="pull-right">
                        <a   href="#" title="View"><i class="fas fa-eye"></i></a>
                        <a  href="javascript:void(0)" class="delete" title="Delete" id="delete{{$d->id}}" data-id="{{$d->id}}"><i class="fas fa-trash-alt" style="color: #c40000;"></i></a>
                        <a  href="#" title="Send message"><i class="fas fa-envelope-square" style="color: green;"></i></a>

                        @if($d->user->isOnline())
                            <i class="fas fa-circle" style="color: #369b40;"></i> Online
                        @else
                            <i class="far fa-circle"></i> Offline
                        @endif

                    </label>
                    <div class="break"></div>
                </li>
            @endforeach



        </ul>
    </div>


@endsection

@section('script')
    <script>
        $(document).on('click','.delete',function () {
            var id=$(this).data('id');
           $.ajax({
               type:'post',
               url:'{{route('managecircle')}}',
               data:{
                   _token:'{{csrf_token()}}',
                   id:id
               },
               success:function () {

                   swal("Succeed!", "User removed from your list.", "success");
                   $('#users').load(location.href +' #users');
               }
           })
        })
    </script>
@endsection