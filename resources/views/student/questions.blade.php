@extends('layouts.master')

@section('content')


    <div class="container-fluid">
        <div class="row justify-content-lg-center">

            <div class="col-8">

                <h4>Question | Topic name {{$question->name}}</h4>

                @php
                    $i=0;
                @endphp
                @foreach($question->question as $b)
                    @php
                        $i++;
                    @endphp
                <div class="card">
                    <div class="card-header">

                        Asked by {{$b->user->name}} on {{$b->created_at->diffForHumans()}}
                    </div>
                    <div class="card-body">
                        <h2>{{$b->question}}</h2>
                        <hr>


                        <div >
                            @foreach($b->comments as $c)
                                <p> <img src="{{asset('image/'.$c->user->image)}}" alt="" width="40px" style="border-radius: 20px"> <strong>{{$c->user->name}} commented</strong>  <small>{{$c->created_at->diffForHumans()}}</small>  <br> <span style="margin-left: 20px">{{$c->comment}}</span> <a href="javascript:void(0)"  data-toggle="collapse" data-target="#demo{{$c->id}}">Reply</a> </p>

                                <div id="demo{{$c->id}}" class="collapse" >
                                    <form action="{{route('postreply')}}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" value="{{$c->id}}" name="commentid">
                                        <p><input type="text" name="reply" ><button type="submit"><i class="fas fa-reply"></i></button></p>
                                    </form>
                                </div>

                                @foreach($c->reply as $r)
                                    <div style="margin-left: 40px">
                                        <p><img src="{{asset('image/'.$r->user->image)}}" alt="" width="20px" style="border-radius: 20px"> {{$r->user->name}} replied <small>{{$c->created_at->diffForHumans()}}</small> <br> <span style="margin-left: 40px">{{$r->reply}}</span> </p>

                                    </div>
                                @endforeach


                            @endforeach


                        </div>


                        <a href=""  data-toggle="collapse" data-target="#collapseExample{{$i}}">Comment</a>

                        <div class="collapse" id="collapseExample{{$i}}">
                            <form action="{{route('postcomment')}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="question_id" value="{{$b->id}}">
                                <input type="hidden" name="author_id" value="{{$b->user->id}}">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="comment" name="comment" placeholder="Write your comment">
                                </div>
                                <button type="submit" class="btn btn-primary">Comment</button>
                            </form>
                        </div>
                    </div>
                </div>
                    &nbsp;
                @endforeach
                &nbsp;

            </div>
        </div>
    </div>

@endsection
