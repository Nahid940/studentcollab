@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-lg-center">

            <div class="col-8">

                <h4>Articles</h4>
                @foreach($blog=\App\BlogPost::with('user')->orderBy('created_at','desc')->get() as $b)
                    <div class="card">

                        <div class="card-header">
                            @if(\Illuminate\Support\Facades\Auth::user()->id==$b->user_id)
                            Posted by you on {{$b->created_at->diffForHumans()}}
                            @else
                            Posted by {{$b->user->name}}{{$b->created_at->diffForHumans()}}
                            @endif
                        </div>

                        <div class="card-body">
                            <img src="{{asset('image/'.$b->image)}}" width="200px" alt="">

                            @php echo '<h3>'.$b->blog.'</h3>'
                            @endphp


                            <div>
                                @foreach($b->comment as $c)
                                    <p><img src="{{asset('image/'.$c->user->image)}}" alt="" width="40px" style="border-radius: 20px"> <strong>{{$c->user->name}} said</strong>  {{$c->comment}} <small>{{$c->created_at->diffForHumans()}}</small></p>
                                @endforeach
                            </div>

                            <a href=""  data-toggle="collapse" data-target="#collapseExample{{$b->id}}">Comment</a>

                            <div class="collapse" id="collapseExample{{$b->id}}">
                                <form action="{{route('postcommentonblog')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="blog_id" value="{{$b->id}}">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="comment" name="comment" placeholder="Write your comment">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Comment</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    &nbsp;
                @endforeach

            </div>
        </div>
    </div>
@endsection