@extends('layouts.master')

@section('css')

    <link href="{{asset('front-end/css/multiple-select.css')}}" rel="stylesheet"/>
@endsection
@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-2">
            </div>

            <div class="col-lg-6">

                <div class="row">
                    <div class="col-lg-12">
                        <form action="" id="52435" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="input-field">
                                <textarea id="textarea1" name="question" class="form-control" placeholder="Write your question"></textarea>
                            </div>
                            &nbsp;

                            <div class="input-field">
                                <label for="user_id">Select topic</label>
                                <select multiple="multiple" name="topic" id="topic" class="form-control" style="width: 50%">
                                    @foreach( $topic as $t))
                                        <option value="{{$t->id}}">{{$t->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            {{--<div class="input-field">--}}
                                {{--<a href="#attach" id="attachfile" data-toggle="collapse"><i class="fas fa-paperclip"></i></a>--}}
                                {{--<div id="attach" class="collapse">--}}
                                    {{--<input type="file" name="files" class="form-control" id="attach" multiple>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="input-field">
                                <button class="btn btn-success" type="button" name="action" id="postquestion">Post</button>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-12">
                        <div id="questions"></div>
                    </div>
                </div>
            </div>


            <div class="col-lg-2">
                <div class="row">
                    <p>Total positive point <i class="fas fa-check-circle" style="color: #00a330;font-size: 20px"title="Total positive point"></i> {{$posmark}}</p>
                   <p>Total negative points <i class="fas fa-times-circle" style="color:#ba0031;font-size: 20px" title="Total negative point"></i> {{$negmark}}</p>
                    <p>Total questions <i class="fab fa-stack-exchange" style="color:#3287ff;font-size: 20px" title="Total asked questions"></i> {{$totalQuestion}}</p>
                </div>
                <hr>

                <div class="row">
                    @foreach($totalquestion as $total)
                        <p>
                            <a href="{{route('ecachQuestion',['id'=>$total->topic_id])}}"><span class="badge badge-secondary" style="margin-left: 5px">{{$total->name}}  </span></a> ×
                                <span class="badge badge-info">{{$total->total_asked}}</span>

                        </p>
                    @endforeach
                </div>


                <hr>
                <div class="row">
                    @if(session('succeed'))
                        <div class="alert alert-primary" role="alert">
                            {{session('succeed')}}
                        </div>
                    @endif
                    <div class="col-lg-12">
                    <p>Share resources</p>
                    <form action="{{url('/resources')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="input-field">
                        <input type="file" name="files[]" class="form-control" multiple>
                            <button class="btn btn-success" type="submit" name="action">Post</button>
                        </div>

                    </form>
                    </div>
                </div>
                <hr>

                <dov class="row">
                    <a href="{{route('favouritequestions')}}"><i class="fas fa-star"></i> Your favourite lists  <span style="color: #ff0037; font-weight: bold">  {{$favouriteLists}} </span></a>
                </dov>
            </div>

        </div>



    </div>
@endsection
@section('script')
    <script src="{{asset('front-end/js/multiple-select.js')}}"></script>

    <script>
        var topic=[];
        var file=[];
        var token='{{\Illuminate\Support\Facades\Session::token()}}';
        var commentid;
        var x;
        var id='{{\Illuminate\Support\Facades\Auth::user()->id}}';
        $('select').multipleSelect();


        $(document).ready(function () {
            alertify.set('notifier','position', 'top-right');
            alertify.success('Student-collab !! Collaborate to learn');
        })

        $(document).on('click','#postquestion',function ( ) {
//            event.preventDefault();
            var question=$('#textarea1').val();
            topic=$('#topic').val();

//            var form_data = new FormData();

//            $("input:checkbox[name=topic]:checked").each(function(){
//                topic.push($(this).val());
//            });
;
//            var filedata = $('#file').get(0).files;

//            $(filedata).each(function (i,myfile) {
//                file.push(myfile);
////                console.log(myfile.name,myfile.size);
////                file=myfile;
//            });
//            console.log(file);

            if(topic.length>0 || question!='') {
                $.ajax({
                    type: 'post',
                    url: '{{route('studentpostquestion')}}',
                    enctype: 'multipart/form-data',
                    data:
                        {
                            _token: token,
                            question: question,
                            topic: topic
                        },
                    success: function (data) {

                        loadQuestions();
                        topic = [];
                        document.getElementById("52435").reset();
                        swal("Succeed!", "Your question has been posted, wait for admin approval.", "success");
                    },
                    error: function (xhr) {
                        alert(xhr.responseText);
                    }
                });
            }else {
                alertify.set('notifier','position', 'top-right');
                alertify.error('Write your question and select topic properly !!.');
            }
        });


        function loadQuestions() {

            $.ajax({
                url: "{{ route('studentquestions') }}",
                type: 'GET',
                success: function(data) {

//                    console.log(data);

                    var allQuestions = JSON.parse(data);
//                    console.log(allQuestions[0][0].user.id);
//                    console.log(allQuestions[0][0].id);
//                    console.log(allQuestions[0][0].favourites);
                    var questionsContainer = $('#questions');
                    var rows = '', showUrl = '', token = '', number = 1;
                    for(var i=0;i<allQuestions[0].length;i++) {
                        rows += '<div class="card" style="margin-top: 10px">' +
                            '<div class="card-header">' +
                            '<a href="user/'+ allQuestions[0][i].user['id']+'"><p>Asked by ' + allQuestions[0][i].user['name'] + ' <img src="image/img_avatar.png" style="width: 30px;height: 30px;border-radius: 10px"></p></div></a><span style="color: #0063a0;">';

                        if(allQuestions[0][i].favourites!=null && allQuestions[0][i].favourites.question_id==allQuestions[0][i].id && id==allQuestions[0][i].favourites.user_id)
                        {
                            rows +="<i class='fas fa-star' style='color: #ff4907;' title='Your favourite'></i>";
                        }

                        for (var x = 0; x < allQuestions[0][i].topic.length; x++) {
                            rows +=' <i class="fas fa-book"></i> '+ allQuestions[0][i].topic[x].name+'</span></p>';
                        }

                        rows += '</p><div class="card-header"><div style="height: 50px">' +
                            '<p><i class="far fa-comment-alt" style="font-size: 20px;margin-top:10px"></i><b>  ' + allQuestions[0][i].question + '</b><p>' +
                            '</div>';

                        rows+='<div><a href="javascript:void(0)"><i class="far fa-check-circle plus" data-id="'+allQuestions[0][i].id+'" style="font-size: 20px"></i></a>' +
                            '<a href="javascript:void(0)"> <i class="far fa-times-circle minus" data-id="'+allQuestions[0][i].id+'" style="font-size: 20px"></i></a>' +
                            '<a href="javascript:void(0)"> <i  class="fas fa-star favourite" data-id="'+allQuestions[0][i].id+'" style="font-size: 20px;color: #03c100"></i></a>' +
                                '</div>'
//                            '</span>';

                        for(var m=0;m<allQuestions[2].length;m++)
                        {
                            if(allQuestions[2][m].question_id==allQuestions[0][i].id)
                            {
                                rows+='<span><p id="marks"><i class="far fa-check-square" style="font-size: 20px;color: green;margin-top:10px"></i>'+allQuestions[2][m].totalpluspoint+'</span>';
                            }
                        }
                        for(var m=0;m<allQuestions[3].length;m++)
                        {
                            if(allQuestions[3][m].question_id==allQuestions[0][i].id)
                            {
                                rows+='<span><i class="fas fa-times" style="font-size: 20px;color: red;margin-top:10px"></i>'+allQuestions[3][m].totalnegativepoint+'<span></p>';
                            }
                        }

                        rows+= '<div class="card card-body" style="margin-left: 30px" id="allcomments">' +
                            '<p class=""> Comments</p>' +
                            '<div class="collection">';


                        //Reply section--------------------------------------------------------------------------------
                        for (x = 0; x < allQuestions[0][i].comments.length; x++) {
                            rows +=
                                '<p><a href= "user/'+ allQuestions[0][i].user['id']+'"><i class="far fa-comments" style="font-size: 20px;color: #00AEEF;margin-left:10px"> </i>  ' + allQuestions[0][i].comments[x].user_name + '</a> said <br>'+
                                '<p style="margin-left: 20px"> ' + allQuestions[0][i].comments[x].comment + '</p>'+


                                '<div class="reply" style="margin-left: 20px">';

                            for (var a = 0; a < allQuestions[1].length; a++) {

                                for (var y = 0; y < allQuestions[1][a].reply.length; y++) {

                                    if (allQuestions[1][a].reply[y].comment_id == allQuestions[0][i].comments[x].id) {

                                        rows += '<p><i class="fas fa-caret-right" style="font-size: 15px;color: #00AEEF"></i> ' + allQuestions[1][a].reply[y].reply + '</p>';
                                    }
                                }

                            }

                            rows += '<div id="replyarea' + allQuestions[0][i].comments[x].id + '" class="collapse"><input style="width: 80%;border: solid 1px #979faf;padding:2px" type="text" id="replytext'+allQuestions[0][i].comments[x].id+'"><button class="btn btn-success" style="padding: 2px;" id="btnreply" data-xcsrf="'+allQuestions[0][i].comments[x].id+'" name="action"><i class="fas fa-reply"></i></button></div>';
                            rows+=
                            '<div style="margin-bottom: 10px;margin-left: 30px"><a data-toggle="collapse" href="#replyarea' + allQuestions[0][i].comments[x].id  + '" id="reply' + allQuestions[0][i].comments[x].id + '" class="replybutton" data-id="' + allQuestions[0][i].comments[x].id + '">Reply</a></div></div><hr>';
                        }

                        rows += '</div>' +
                            '</div>' +
                            '<a style="text-decoration: none" href="#commentbox' + allQuestions[0][i].id + '" data-toggle="collapse"><i class="fas fa-comment-alt"></i> Comment</a>' +
                        '<div id="commentbox' + allQuestions[0][i].id + '" class="collapse">' +
                        '<input type="hidden" class="author_id" id="author_id'+allQuestions[0][i].user['id']+'"  name="author_id" value="'+allQuestions[0][i].user['id']+'">'+
                        '<input type="text" class="form-control" name="comments" id="comments' + allQuestions[0][i].id + '" style="border-radius: 15px" placeholder="Comment"><button class="btn btn-info" data-id="' + allQuestions[0][i].id + '" data-id1="'+allQuestions[0][i].user['id']+'" id="btncomment" type="submit" name="action" style="margin-top: 10px">Comment</button>' +
                        '</div>';

                        rows +=
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }

                    questionsContainer.html(rows);
                },

                error: function(xhr) {
                    alert(xhr.responseText);
                }

            });
        }

        $(document).ready(function () {
//            timeOut();
            loadQuestions();
        });

//        function timeOut(){
//            setTimeout(function(){
//               loadQuestions();
//                timeOut();
//            },5000);
//        }



        $(document).on('click','#btncomment',function () {
            var val=($(this).data('id'));
            var author_id=($(this).data('id1'));
            var comment=$('#comments'+val).val();
            if(comment!=''){

            $.ajax({
                url: "{{ route('postcomment') }}",
                type: 'post',
                data:{
                    question_id:val,
                    comment:comment,
                    author_id:author_id,
                    _token:token
                },
                success:function (data) {
                    $('#allcomments'+val).append('<p>'+comment+'</p>');
                    $('#comments'+val).val('');
                    loadQuestions();
                }
            });

            }else{
                $('#comments'+val).css('border-color','red');
            }
        });

//        $(document).on('click','.replybutton',function () {
//            commentid=($(this).data('id'));
////            alert(commentid);
//            $('#reply'+commentid).html('<div class="input-group"><input class="form-control" type="text" id="replytext'+commentid+'"><button class="btn btn-info" id="btnreply" data-xcsrf="'+commentid+'" name="action">Reply</button></div>');
//        });

        $(document).on('click','#btnreply',function () {
            var val=($(this).data('xcsrf'));
            var replytext=($('#replytext'+val).val());
//            alert(val);

            if(val!='' && replytext!='')
            {
                $.ajax({
                    type:'post',
                    url:'{{route('postreply')}}',
                    data:{
                        _token:token,
                        commentid:val,
                        reply:replytext
                    },
                    success:function (data) {
//                        console.log(data);
                        loadQuestions();
                    },
                    error: function(xhr) {
                        alert(xhr.responseText);
                    }
                })
            }

        });

        $(document).on('click','.plus',function () {
//            ($(this).data('id'));
//            alert(($(this).data('id')));
            if($(this).data('id')!=''){
                $.ajax({
                    type:'post',
                    url:'{{route('mark')}}',
                    data: {
                        _token: token,
                        question_id:($(this).data('id')),
                        point:1
                    },
                    success:function (data) {
//                        console.log(data);
                        loadQuestions();

                    },
                    error: function (request, status, error) {
                        alert(request.responseText);
                    }

                })
            }

        });

        $(document).on('click','.minus',function () {
            if($(this).data('id')!=''){
                $.ajax({
                    type:'post',
                    url:'{{route('mark')}}',
                    data: {
                        _token: token,
                        question_id:($(this).data('id')),
                        point:0
                    },
                    success:function (data) {
//                        console.log(data);
                        loadQuestions();
//                        $('#minus'+$(this)).css('color',"red");
                    },
                    error: function (request, status, error) {
                        alert(request.responseText);
                    }

                })
            }
        });

        $(document).on('click','.favourite',function () {
            if($(this).data('id')!=''){
                $.ajax({
                    type:'post',
                    url:'{{route('favourite')}}',
                    data: {
                        _token: token,
                        question_id:($(this).data('id')),
                    },
                    success:function (data) {
//                        console.log(data);
                        loadQuestions();
//                        $('#minus'+$(this)).css('color',"red");
                    },
                    error: function (request, status, error) {
                        alert(request.responseText);
                    }

                })
            }
        });

    </script>
@endsection
