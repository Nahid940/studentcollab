@extends('layouts.master')

@section('css')

    <link href="{{asset('front-end/css/multiple-select.css')}}" rel="stylesheet"/>
@endsection

@section('content')



    <div class="container">

                    <ul class="list-group" id="groups">
                        <li href="#" class="list-group-item title">
                            <label for=""><h3>Your groups</h3></label>

                            <button type="button" class="btn btn-outline-primary"  data-toggle="modal" data-target="#myModal"> <i class="fas fa-plus-square"></i> Create new group</button>
                        </li>

                        @foreach($data as $d)
                            <li href="#" class="list-group-item text-left" style="float: left" i>
                                <label class="name">
                                    <a href="{{route('viewgroup',['id'=>$d->id])}}"> {{$d->group_name}} </a><br>
                                </label>
                                <label class="pull-right">
                                    <a  href="{{route()}}" title="Delete"><i class="fas fa-trash-alt" style="color: #c40000;"></i></a>
                                </label>
                                <div class="break"></div>


                            </li>
                        @endforeach

                    </ul>
        &nbsp;
                    <ul class="list-group">
                        <li href="#" class="list-group-item title">
                            <h3>All the groups you belong to</h3>
                        </li>

                        @if(sizeof($datas->member )>=1)
                            @foreach($datas->member as $d)
                                    <li href="#" class="list-group-item text-left" style="float: left">
                                        <label class="name">
                                            <a href="{{route('viewgroup',['id'=>$d->id])}}"> {{$d->group_name}} </a><br>
                                        </label>

                                        <label class="pull-right">
                                            <a   href="#" title="View"><i class="fas fa-eye"></i></a>
                                            <a  href="#" title="Leave group"><i  class="fas fa-sign-out-alt" style="color: green;"></i></a>
                                        </label>

                                        <div class="break"></div>
                                    </li>

                            @endforeach
                        @else

                            <li href="#" class="list-group-item title">
                                <span style="color: #ef2d2d;">You don't belong to any group !!</span>
                            </li>

                        @endif
                    </ul>



        @php


        @endphp
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Create Group</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="email">Group name:</label>
                                    <input type="text" class="form-control" id="group_name" required>
                                </div>

                                <div class="form-group">

                                    <label for="user_id">Select People</label>
                                    <select multiple="multiple" style="width: 100%" name="user_id" id="user_id">
                                        @foreach( $circle as $c))
                                        <option value="{{$c->user->id}}">{{$c->user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" id="CreateGroup" class="btn btn-success" >Create group</button>
                                <button type="button"  class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
    </div>





@endsection
@section('script')
    <script src="{{asset('front-end/js/multiple-select.js')}}"></script>
    <script>

        $('select').multipleSelect();

        $(document).on('click','#CreateGroup',function () {
           var id=$('#user_id').val();

           $.ajax({
               type:'post',
               url:'{{route('createGroup')}}',
               data:{
                   _token:'{{csrf_token()}}',
                   id:id,
                   group_name:$('#group_name').val()
               },
               success:function (data) {
                   $('#groups').load(window.location.href + ' #groups');
                   $('#myModal').modal('hide');
               },
               error:function (t) {
                   alert(t.responseText);
               }

           })
        })
    </script>
@endsection