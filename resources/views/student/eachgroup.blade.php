@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">


            <div class="card">
                <div class="card-header">
                    <h2>{{$Groupdata->group_name}}</h2>
                </div>
                <div class="card-body">

                    @if($Groupdata->user_id==\Illuminate\Support\Facades\Auth::user()->id)
                        <form id="submitPost" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <textarea id="mytextarea" name="texts"></textarea>
                            <input type="hidden" name="group_id" value="{{$Groupdata->id}}">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Attach file</label>
                                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image[]" multiple="true">
                                    </div>
                                </div>

                                <div class="col-md-6">

                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" id="submit" class="btn btn-info">Post</button>
                            </div>
                        </form>
                    @endif

                </div>
            </div>



            <div class="card">
                <div class="card-header">
                    <h2>Discussion of {{$Groupdata->group_name}} group</h2>
                </div>

                <div id="discussions">

                </div>

            </div>

        </div>

        <div class="col-md-4">

                <ul class="list-group">
                    <li href="#" class="list-group-item title">
                        <label for="">Members of this group</label>
                    </li>

                    @foreach($Groupdata->groupmember as $g)


                        <li href="#" class="list-group-item text-left" style="float: left">
                            <label class="name">
                                <a href="{{route('users',['id'=>$g->id])}}"> {{$g->name}} </a><br>
                            </label>
                            <label class="pull-right">
                                <a   href="#" title="View"><i class="fas fa-eye"></i></a>
                                <a  href="#" title="Delete"><i class="fas fa-trash-alt" style="color: #c40000;"></i></a>
                                <a  href="#" title="Send message"><i class="fas fa-envelope-square" style="color: green;"></i></a>
                                @if($g->isOnline())
                                    <i class="fas fa-circle" style="color: #369b40;"></i> Online
                                @else
                                    <i class="far fa-circle"></i> Offline
                                @endif
                            </label>
                            <div class="break"></div>
                        </li>
                    @endforeach
                </ul>

        </div>
    </div>
</div>
@endsection

@section('script')

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>


    <script>

        $(document).ready(function () {
            loaddiscussion();
        })


        $('#submitPost').on('submit', function(e)  {

            tinyMCE.get("mytextarea").save();
//           var text=(tinyMCE.activeEditor.getContent());
            e.preventDefault();

           $.ajax({
               type:'post',
               url:'{{route('postDiscussion')}}',
               {{--data:{--}}
                   {{--_token:'{{csrf_token()}}',--}}
                   {{--discussion:text,--}}
                   {{--group_id:'{{$Groupdata->id}}'--}}
               {{--},--}}
               data:  new FormData(this),
               contentType: false,
               processData:false,

               success:function (data) {
                   console.log(data);
               },
               error:function (x) {
                   alert(x.responseText);
               }
           })
        })

        function loaddiscussion() {
        $.ajax({
            type:'get',
            url:'{{route('getAllDiscussion',['id'=>$Groupdata->id])}}',
            success:function (data) {
//                console.log(data[0].file.length);
            var rows='',x,i=0;

            for( x=0;x<data.length;x++)
            {
                i++;
               rows+= '<div class="card-body">';


                for(l=0;l<data[x].file.length;l++) {
                    rows += '<p><img  style="width: 200px; float: left;margin: 5px"  src="/image/image/' + data[x].file[l].file + '" alt="Card image cap"></p>';
                }

                    rows+=   '<p class="card-subtitle mb-2 text-muted">Discussion '+i+'</p>'+
                        '<p class="card-text">'+data[x].discussion+'</p>';
                rows += '<label for="" style="color: #47bcf7">Comments</label>';

                for(l=0;l<data[x].comment.length;l++) {
                    rows += '<div style="margin-left: 20px;margin-bottom: 5px"><p class="card-text"><i class="far fa-comments"></i> '+ data[x].comment[l].user_name +' commented '+data[x].comment[l].comment+'</p></div><hr>';
                }

               rows+= '<p class="card-text"> <a href="javascript:void(0)" data-toggle="collapse" data-target="#collapseOne'+data[x].id+'" >Comment</a>' +
                   '</p><span class="collapse" id="collapseOne'+data[x].id+'"><label for="">Write comment</label><input type="text"  class="form-control" id="commenttext'+data[x].id+'">' +
                   '<div style="margin-top: 15px"><button class="btn btn-info postcomment" data-id="'+data[x].id+'" id="postcomment'+data[x].id+'">Post</button></div></span></div>';


            }
            $('#discussions').html(rows);
            }

        })
        }

        $(document).on('click','.postcomment',function () {
            var id=($(this).data('id'));

            $.ajax({
                type:'post',
                url:'{{route('comment')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    comment:$('#commenttext'+id).val(),
                    id:id
                },
                success:function (data) {
//                    console.log(data);
                    loaddiscussion();
                },
                error:function (d) {
                    alert(d.responseText);
                }
            })
        })
    </script>

@endsection