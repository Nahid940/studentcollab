@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-lg-center">

            <div class="col-8">

                <div class="card">

                    <div class="card-header">
                       <h4>Question asked by you on {{$b->created_at->diffForHumans()}}</h4>
                    </div>

                    <div class="card-body">
                        <h5><i class="fas fa-question"></i> {{$b->question}}</h5>

                        &nbsp;
                        <div>
                            @foreach($b->comments as $c)
                                <p><img src="{{asset('image/'.$c->user->image)}}" alt="" width="40px" style="border-radius: 20px"> <strong>{{$c->user->name}} said</strong>  <br> {{$c->comment}}  <small>{{$c->created_at->diffForHumans()}}</small></p>

                                <div style="margin-left: 40px;color: #7a7070;">
                                    <p><i class="fas fa-reply"></i> Replies</p>
                                    @foreach($c->reply as $r)
                                        <p style="background-color: #f4f4f4"><img src="{{asset('image/'.$r->user->image)}}" alt="" width="20px" style="border-radius: 20px"> <strong>{{$r->user->name}} replied {{$r->created_at->diffForHumans()}}</strong> <br> {{$r->reply}}</p>
                                    @endforeach
                                    <a href="javascript:void(0)"  data-toggle="collapse" data-target="#demo{{$c->id}}">Reply</a>

                                    <div id="demo{{$c->id}}" class="collapse">
                                        <form action="{{route('postreply')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$c->id}}" name="commentid">
                                            <p><input type="text" name="reply" ><button type="submit"><i class="fas fa-reply"></i></button></p>
                                        </form>
                                    </div>
                                </div>
                            @endforeach


                        </div>

                        <a href=""  data-toggle="collapse" data-target="#collapseExample{{$b->id}}">Comment</a>

                        <div class="collapse" id="collapseExample{{$b->id}}">
                            <form action="{{route('postcommentonblog')}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="blog_id" value="{{$b->id}}">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="comment" name="comment" placeholder="Write your comment">
                                </div>
                                <button type="submit" class="btn btn-primary">Comment</button>
                            </form>
                        </div>
                    </div>
                </div>
                &nbsp;

            </div>
        </div>
    </div>

@endsection