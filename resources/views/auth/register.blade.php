@extends('layouts.master')

@section('css')
    <style>

        .showTopic{
            display: none;
        }
    </style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row justify-content-lg-center">
        <div class="col-lg-8  offset-4">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Register</h4></div>

                <span class="label label-danger" id="typeofuser"></span>


                <div class="panel-body">


                    <div class="radio">
                        <label><input type="radio" name="optradio" id="expert">Join as a teacher/expert</label>
                    </div>

                    <div class="radio">
                        <label><input type="radio" name="optradio"  id="student">Join as a student/learner</label>
                    </div>

                    <form class="form-horizontal" method="POST" action="{{ route('register') }}" enctype="multipart/form-data" onsubmit="return checkType()">
                        {{ csrf_field() }}
                        <input type="hidden" id="type" name="type">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block" style="color: red">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Image</label>
                            <div class="col-md-6">

                                    <input type="file" class="form-control" name="image">
                            </div>
                        </div>

                        <div class="form-group " id="teacher" >
                            <label for="" class="col-md-4 control-label">Select topic</label>
                            <div class="col-sm-6">
                                <div class="form-check">
                                    @foreach($topic=\App\Topic::all() as $t)
                                        <label class="form-check-label">
                                            <input type="checkbox" value="{{$t->id}}" name="topic[]">{{$t->name}}
                                        </label>
                                    @endforeach
                                </div>
                            </div>

                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" >
                                    Register
                                </button>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script>
        $(document).ready(function () {
            $('#teacher').addClass('showTopic');
        })


        $(document).on('click','#expert',function () {
        $('#teacher').removeClass('showTopic');
            $('#type').val('teacher');
            $('#typeofuser').hide();
        });



        $(document).on('click','#student',function () {
            $('#type').val('student');
            $('#teacher').addClass('showTopic');
            $('#typeofuser').hide();
        });


        function checkType() {
            if($('#type').val()=='')
            {
                $('#typeofuser').show();
               $('#typeofuser').text('Please select your account type !!');
                $('#typeofuser').css('color','red');
                return false;
            }
            return true;
        }

    </script>

@endsection
