
@extends('layouts.master')
@section('content')

    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-6  offset-3">


                @if(session('successRegistartion'))
                    <div class="alert alert-success">
                        {{session('successRegistartion')}}
                    </div>
                @endif


                <center><h2>Login</h2></center>

                @if ($errors->has('email') || $errors->has('password'))
                    <span class="help-block" style="color: #f22e2e;">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

                <form action="{{ route('login') }}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input  id="email" type="email" placeholder="Email" class="form-control" value="{{ old('email') }}" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input  id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Remember me</label>
                        <a href="">Forgot password</a>
                    </div>
                    <div class="form-group">

                        <button type="submit" class="btn btn-primary ">Login</button> or
                        <a  href="{{route('register')}}">Join</a>

                    </div>

                </form>
            </div>

        </div>
    </div>


@endsection

@section('script')

    <script>

        $(document).ready(function() {
            $('#validator').hide();
            setTimeout(function() {
                $('.colorOrange').fadeOut('slow');
            }, 2000);

            $('#email').keyup(function () {
                var $this = $(this);
                var insertedVal = $this.val();
                if (insertedVal != '') {
                    $('#email').css('box-shadow', '0 0 10px green');
                    $this.css({"color": "green", "border": "1px solid green"});
                } else {
                    $('#email').css('box-shadow', '0 0 10px #ff0000');
                    $this.css({"color": "green", "border": "1px solid #ff0000"});
                }
            });

            $('#password').keyup(function () {
                var $this = $(this);
                var insertedVal = $this.val();
                if (insertedVal != '') {
                    $('#password').css('box-shadow', '0 0 10px green');
                    $this.css({ "border": "1px solid green"});
                } else {
                    $('#password').css('box-shadow', '0 0 10px #ff0000');
                    $this.css({ "border": "1px solid #ff0000"});
                }
            });
        });

        function validateForm()
        {
            setTimeout(function() {
                $('.colorOrange').fadeOut('slow');
            }, 2000);

            var mailformat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var email=document.forms["loginform"]["email"].value;
            var password=document.forms['loginform']['password'].value;

            if(resident_id==''  || password==''){
                $('#validator').show();
                $('#validator').text("Provide each information properly !!");
                return false;
            }else{
                return true;
            }
        }
    </script>

@endsection

