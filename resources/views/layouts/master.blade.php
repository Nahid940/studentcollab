
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Student-Collab</title>


    {{--<link rel="stylesheet" href="{{asset('front-end/css/bootstrap.min.css')}}">--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="{{asset('css/scrolling-nav.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>


    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    <style>
        a:link{
            text-decoration: none;
        }
        body {
            font-family: 'Nanum Gothic', sans-serif;
            font-size: 14px;
            margin-top: 80px;
        }

        h5 {
            font-size: 1.28571429em;
            font-weight: 700;
            line-height: 1.2857em;
            margin: 0;
        }

        .card {
            font-size: 1em;
            overflow: hidden;
            padding: 0;
            border: none;
            border-radius: .28571429rem;
            box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
        }

        .card-block {
            font-size: 1em;
            position: relative;
            margin: 0;
            padding: 1em;
            border: none;
            border-top: 1px solid rgba(34, 36, 38, .1);
            box-shadow: none;
        }

        .card-img-top {

            width: 100%;
            height:10%
        }

        .card-title {
            font-size: 1.28571429em;
            font-weight: 700;
            line-height: 1.2857em;
        }

        .card-text {
            clear: both;
            margin-top: .5em;
            color: rgba(0, 0, 0, .68);
        }

        .card-footer {
            font-size: 1em;
            position: static;
            left: 0;
            padding: .75em 1em;
            color: rgba(0, 0, 0, .4);
            border-top: 1px solid rgba(0, 0, 0, .05) !important;
            background: #fff;
        }

        .card-inverse .btn {
            border: 1px solid rgba(0, 0, 0, .05);
        }

        .profile {
            box-sizing: border-box;
            width: 200px;
            height: auto;
            margin: 0;
            border: 1px solid #fff;


        }

        .profile-avatar {
            margin-top: -200px;
            display: block;
            width: 200px;
            height: auto;


        }

        .profile-inline ~ .card-title {
            display: inline-block;
            margin-left: 4px;
            vertical-align: top;
        }

        .footer{
            margin-top: 50px;
            margin-bottom: 50px;
        }
        .swal-modal {
            border: 3px solid white;
            color: #000;
            font-weight: bold;
            width: 200px;
        }

    </style>
    @yield('css')


</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
        @guest


        <a class="navbar-brand js-scroll-trigger" href="{{route('index')}}">Student-Collab</a>
        @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('student'))

            <a class="navbar-brand js-scroll-trigger" href="{{route('studentindex')}}">Student-Collab</a>
        @else
            <a class="navbar-brand js-scroll-trigger" href="{{route('teacherindex')}}">Student-Collab</a>

        @endguest


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @guest
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{route('login')}}">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{route('register')}}">Join</a>
                </li>
                @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('student'))

                <li class="nav-item">
                    <div class="dropdown">
                        @php
                            $newComment=\App\Notification::with('user')->where('type','teacher')->where('author_id',Auth::user()->id)->where('check','no')->get();
                        @endphp
                        @if(sizeof($newComment)!=null)

                        <span title="Notification" data-toggle="dropdown" style="cursor: pointer;"><i class="fas fa-bell" style="color: white;font-size: 25px"></i> <span class="badge badge-danger"> {{sizeof($newComment)}}</span></span>
                        <div class="dropdown-menu">
                            @foreach($newComment as $n)
                                <a class="dropdown-item" href="{{route('eachquestion',['id'=>$n->post_id,'id1'=>$n->id,'x'=>time()])}}" style="font-size: 15px">{{$n->user_name}} commented on your post.</a>
                            @endforeach
                        </div>
                        @endif
                    </div>
                </li>

                    <li class="nav-item" id="circle">
                        <div class="dropdown">

                            <span title="Notification" data-toggle="dropdown" style="cursor: pointer;">
                                <i class="fas fa-users" style="color: white;font-size: 25px"></i>
                                <span class="badge badge-danger"> {{\App\Circle::where('user_id1',\Illuminate\Support\Facades\Auth::user()->id)->where('accept','no')->count()}}</span>
                            </span>

                            @php
                                $newFriend=\App\Circle::with('user1')->where('user_id1',\Illuminate\Support\Facades\Auth::user()->id)->where('accept','no')->get();
                            @endphp

{{--                            @if(sizeof($newFriend)>=1)--}}
                                <div class="dropdown-menu">
                                    @foreach($newFriend as $nf)
                                        <p><a  class="dropdown-item"><img src="{{asset('image/'.$nf->user1->image)}}" style="border-radius: 5px" width="20px" alt=""> {{$nf->user1->name}} wants to be your mate.</a>
                                            <div style="float: right; cursor: pointer">
                                                <span class="badge badge-success accept" data-id="{{$nf->id}}"><i class="fas fa-check"></i> Approve</span>
                                                <span class="badge badge-danger decline" data-id="{{$nf->id}}"><i class="fas fa-times"></i> Decline</span>
                                            </div>
                                        </p>
                                    @endforeach
                                </div>
                            {{--@else--}}

                            {{--@endif--}}


                        </div>
                    </li>

                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{route('group')}}">Your Groups</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{route('studentaccesstoblog')}}">Experts blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{route('circle')}}">Your circle</a>
                </li>
                <li class="nav-item">
                        <a class="nav-link js-scroll-trigger"  href="{{route('logout')}}">{{\Illuminate\Support\Facades\Auth::user()->name}} Logout</a>
                </li>

                @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('teacher'))


                    <div class="dropdown">
                        @php
                            $newComment=\App\Notification::with('user')->where('type','studentcomment')->where('author_id',Auth::user()->id)->where('check','no')->get();
                        @endphp
                        @if(sizeof($newComment)!=null)

                            <span title="Notification" data-toggle="dropdown" style="cursor: pointer;"><i class="fas fa-bell" style="color: white;font-size: 25px"></i> <span class="badge badge-danger"> {{sizeof($newComment)}}</span></span>
                            <div class="dropdown-menu">
                                @foreach($newComment as $n)
                                    {{--@if($n->type=='teacher')--}}
                                        {{--<a class="dropdown-item" href="{{route('eachblog',['id'=>$n->post_id,'id1'=>$n->id,'x'=>time()])}}" style="font-size: 15px"> <i class="fab fa-jenkins"></i> {{$n->user_name}} commented on your post.</a>--}}
                                    {{--@else--}}
                                        <a class="dropdown-item" href="{{route('eachblog',['id'=>$n->post_id,'id1'=>$n->id,'x'=>time()])}}" style="font-size: 15px"> <i class="far fa-user"></i> {{$n->user_name}} commented on your post.</a>
                                    {{--@endif--}}

                                @endforeach
                            </div>
                        @endif
                    </div>

                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{route('blog')}}">Your blog</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger"  href="{{route('logout')}}">Logout</a>
                    </li>
                    @endguest
                </ul>


            </ul>
        </div>

    </div>
</nav>



@yield('content')

<footer class="py-5 bg-dark footer">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
{{--<script src="vendor/jquery/jquery.min.js"></script>--}}
{{--<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>





<!-- Plugin JavaScript -->
{{--<script src="{{asset('front-end/js/jquery.easing.min.js')}}"></script>--}}

<!-- Custom JavaScript for this theme -->
<script src="{{asset('front-end/js/scrolling-nav.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>

@yield('script')

<script>
    $(document).on('click','.accept',function () {
        var id=$(this).data('id');
        $.ajax({

            type:'post',
            url:'{{route('addOrRemove')}}',
            data:{
                action:'accept',
                _token:'{{csrf_token()}}',
                id:id
            },
            success:function (data) {
                swal({
                    text: "Approved",
                    icon: "success",
                });
                $('#circle').load(window.location.href + ' #circle');
            }
        })
    })

    $(document).on('click','.decline',function () {
        var id=$(this).data('id');
        $.ajax({

            type:'post',
            url:'{{route('addOrRemove')}}',
            data:{
                action:'decline',
                _token:'{{csrf_token()}}',
                id:id
            },
            success:function (data) {
                swal({
                    text: "Declined",
                    icon: "warning"
                });
                $('#circle').load(window.location.href + ' #circle');
            }
        })

    })
</script>

</body>

</html>