@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="row" style="margin-top: 100px">
            <div class="col-lg-8 offset-2">
                <div class="row">
                    <div class="card card-inverse card-info">
{{--                        <img class="card-img-top" src="{{asset('image/'.$data->image)}}" >--}}
                        <img src="{{asset('image/Blue-Gradient-Color.png')}}" alt="Girl in a jacket">
                        <div class="card-block">
                            <figure class="profile">
                                <img src="{{asset('image/'.$data->image)}}" class="profile-avatar" alt="">
                            </figure>
                            <h4 class="card-title mt-3">{{$data->name}}</h4>
                            <div class="meta card-text">
                                <a>Friends</a>
                                @if($data->roles[0]->role_name=='student')
                                <h4><span class="badge badge-success"><i class="far fa-user"></i> Student</span></h4>
                                    @elseif($data->roles[0]->role_name=='teacher')
                                   <h4> <span class="badge badge-success"> <i class="far fa-user"></i> Teacher</span></h4>
                                @endif
                            </div>
                            <div class="card-text">
                                {{$data->address}}
                                Email: {{$data->email}}
                            </div>
                        </div>

                        @if($d!=null && $d!='self')
                        <div class="card-footer">
                            {{--@if($d!=3)--}}
                                {{--@if($d==2)--}}
                                    {{--<a href="{{route('addnew',['id'=>$data->id])}}" class="btn btn-info float-right btn-sm">Add to your friend list</a>--}}
                                {{--@elseif($d==0)--}}
                                    {{--<h2> <span class="badge badge-success float-right"> <i class="fas fa-check"></i> In your circle</span></h2>--}}
                                {{--@elseif($d==1)--}}
                                    {{--<a href="javascript:void(0)" class="btn btn-info float-right btn-sm">Pending</a>--}}
                                {{--@elseif($d==4)--}}
                                    {{--<a href="{{route('addnew',['id'=>$data->id])}}" class="btn btn-info float-right btn-sm">Accept</a>--}}
                                {{--@endif--}}
                            {{--@endif--}}
                            @if($d->accept=='yes')
                                <a href="" class="btn btn-info float-right btn-sm">In your circle</a>
                            @elseif($d->accept=='no' && ($d->user_id==\Illuminate\Support\Facades\Auth::user()->id))
                                <a href="" class="btn btn-info float-right btn-sm">Pending</a>
                            {{--@elseif($d->accept=='no' && ($d->user_id==\Illuminate\Support\Facades\Auth::user()->id))--}}
                                {{--<a href="" class="btn btn-info float-right btn-sm">Accept</a>--}}
                            @elseif($d->accept=='no' && ($d->user_id1==\Illuminate\Support\Facades\Auth::user()->id))
                                <a href="" class="btn btn-info float-right btn-sm">Accept</a>
                            @endif
{{--                            {{route('addnew',['id'=>$data->id])}}--}}
                        </div>


                            @if($d->accept=='yes')
                                <ul id="tabsJustified" class="nav nav-tabs">
                                    {{--<li class="nav-item"><a href="" data-target="#profile1" data-toggle="tab" class="nav-link small text-uppercase active">Profile</a></li>--}}
                                    <li class="nav-item"><a href="" data-target="#home1" data-toggle="tab" class="nav-link small text-uppercase active">All posts</a></li>
                                    <li class="nav-item"><a href="" data-target="#archive" data-toggle="tab" class="nav-link small text-uppercase">Resource archive</a></li>
                                    <li class="nav-item"><a href="" data-target="#messages1" data-toggle="tab" class="nav-link small text-uppercase">Send message</a></li>
                                </ul>
                            @endif

                        @elseif($d=='self')

                        @else
                            <div class="card-footer">
                                <a href="{{route('addnew',['id'=>$data->id])}}" class="btn btn-info float-right btn-sm">Add to your friend list</a>
                            </div>
                        @endif

                        <div id="tabsJustifiedContent" class="tab-content">
                                <div id="home1" class="tab-pane fade">
                                    <div class="list-group">

                                    </div>
                                </div>

                            <div id="profile1" class="tab-pane fade active show">
                                {{--<div class="row pb-12">--}}
                                    {{--<div class="col-md-7">--}}
                                            {{--<a href="" class="link">more</a> information as needed.--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-5"><img src="//dummyimage.com/1005x559.png/5fa2dd/ffffff" class="float-right img-fluid img-rounded"></div>--}}

                                    @foreach($questions as $q)
                                        <div class="card">
                                            <div class="card-body">
                                                <b>{{$data->name}} </b>asked {{$q->created_at->diffForHumans()}}
                                                <p>{{$q->question}}</p>

                                                <a  data-toggle="collapse" href="#comments{{$q->id}}" >Comments</a>
                                                <div style="margin-left: 50px" class="collapse multi-collapse" id="comments{{$q->id}}">
                                                    <hr>
                                                    @foreach($q->comments as $c)
                                                        <p>{{$c->comment}}</p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                {{--</div>--}}
                            </div>

                                <div id="archive" class="tab-pane fade">
                                    <div class="list-group">
                                        @foreach(\App\Resources::with('user')->where('user_id',\Illuminate\Support\Facades\Auth::user()->id)->get() as $r)
                                            <a href="{{asset('image/'.$r->file_name)}}" class="list-group-item d-inline-block">{{$r->file_name}} <i class="fas fa-download"></i></a>
                                        @endforeach
                                    </div>
                                </div>

                            <div id="messages1" class="tab-pane fade">
                                <div class="list-group">
                                    <div class="card">
                                        <div class="card-header">Header</div>
                                        <div class="card-body">


                                        </div>
                                        <div class="card-footer">Footer</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        @if(session('requestSend'))
        $(document).ready(function () {
            swal("Succeed!",'{{session('requestSend')}}',"success");
        })
        @endif
    </script>
@endsection