@extends('layouts.master')


@section('content')

	<div class="container-fluid">
		<div class="row justify-content-lg-center">

				<div class="col-8">
					<h4>All the questions</h4>

					@foreach($data as $d)
						<div class="card">
							<div class="card-header">
								<span style="color: #1a8ff4;">Question asked by {{$d->user->name}} | {{$d->created_at->diffForHumans()}}</span>
							</div>

							<div class="card-body">
								<p class="card-text">{{$d->question}}</p>
								<a data-toggle="collapse" href="#collapseExample{{$d->id}}" role="button" aria-expanded="false" aria-controls="collapseExample">
									Comments
								</a>
								<div id="collapseExample{{$d->id}}" class="collapse">
									@foreach($d->comments as $cmnt)
										<ul class="list-group">
											<li class="list-group-item"><span  style="color: #1a8ff4;">{{$cmnt->user->name}} commented on {{$cmnt->created_at->diffForHumans()}}</span><br> {{$cmnt->comment}} <a href="{{route('login')}}" style="float: right">Post comment</a></li>
											@if(sizeof($cmnt->reply)>=1)
													<a data-toggle="collapse" href="#reply{{$cmnt->id}}" role="button" aria-expanded="false" aria-controls="reply">
														Replies
													</a>
											@endif
										</ul>

										&nbsp;

										<div style="margin-left: 30px">
											<div id="reply{{$cmnt->id}}" class="collapse">
												@foreach($cmnt->reply as $rply)
													<ul class="list-group">
														<li class="list-group-item"><span  style="color: #1a8ff4;">{{$rply->user_name}} replied on {{$rply->created_at->diffForHumans()}}</span><br> {{$rply->reply}}</li>
													</ul>
													&nbsp;
												@endforeach
											</div>


										</div>


									@endforeach

								</div>

							</div>
						</div>
						&nbsp;
					@endforeach


				</div>

		</div>

	</div>


@endsection