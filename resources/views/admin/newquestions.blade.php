@extends('admin.layout.master')

@section('content')
    <h2>View data</h2>
    @if(session('approved'))
        <div class="alert alert-info">{{session('approved')}}</div>
    @endif
    @if(session('deleted'))
        <div class="alert alert-danger">{{session('deleted')}}</div>
    @endif

    <table class="table">
        <thead>
            <tr>
                <th>Person Name</th>
                <th>Email</th>
                <th>Question</th>
                <th>Topics</th>
                <th>Approve</th>
                <th>Remove</th>
            </tr>
        </thead>
        @foreach($data as $d)
        <tbody>
            <tr>
                <td>{{$d->user->name}}</td>
                <td>{{$d->user->email}}</td>
                <td>{{$d->question}}</td>
                <td>
                    @foreach($d->topic as $t)
                        {{$t->name}}
                    @endforeach
                </td>
                <td><a href="{{route('approvequestion',['id'=>$d->id])}}"  class="btn btn-success">Approve</a></td>
                <td><a href="{{route('removequestion',['id'=>$d->id])}}"  class="btn btn-danger">Remove</a></td>

            </tr>
        </tbody>
            @endforeach

    </table>


    @endsection
