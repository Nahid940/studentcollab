@extends('admin.layout.master')

@section('content')

 <dov class="container-fluid">
     @if(session('delete_confirm'))
         <div class="alert alert-success">
             {{session('delete_confirm')}}
         </div>
     @endif

         @if(session('approve'))
         <div class="alert alert-success">
             {{session('approve')}}
         </div>
     @endif
     <table class="table">
         <thead>
         <tr>
             <th>Person Name</th>
             <th>Email</th>
             <th>Image</th>
             <th>Role</th>
             <th>Approve</th>
             <th>Remove</th>
         </tr>
         </thead>
         @foreach($allUsers as $d)
             <tbody>
                 <tr>
                     <td>{{$d->name}}</td>
                     <td>{{$d->email}}</td>
                     <td><img src="{{asset('image/'.$d->image)}}" width="80px" alt=""></td>
                     <td>@if($d->hasRole('teacher'))
                             Teacher
                         @elseif($d->hasRole('student'))
                             Student
                         @endif
                     </td>
                     {{--<td><a href="{{route('approvequestion',['id'=>$d->id])}}"  class="btn btn-success">Approve</a></td>--}}
                     @if($d->approve=='no')
                      <td><a href="{{route('approve',['id'=>$d->id])}}"  class="btn btn-warning"><i class="fa fa-check"></i></a></td>
                     @else
                     <td><a class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i></a></td>
                     @endif
                     <td><a href="{{route('removeUser',['id'=>$d->id])}}"  class="btn btn-danger"><i  class="fa fa-trash-o"aria-hidden="true"></i></a></td>
                 </tr>
             </tbody>
         @endforeach

     </table>
 </dov>

@endsection