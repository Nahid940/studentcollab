<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> StudentCollab | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('back-end/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('back-end/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
{{--    <link rel="stylesheet" href="{{asset('back-end/bower_components/Ionicons/css/ionicons.min.css')}}">--}}
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('back-end/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('back-end/dist/css/skins/_all-skins.min.css')}}">
    <!-- Morris chart -->
{{--    <link rel="stylesheet" href="{{asset('back-end/bower_components/morris.js/morris.css')}}">--}}
    <!-- jvectormap -->
{{--    <link rel="stylesheet" href="{{asset('back-end/bower_components/jvectormap/jquery-jvectormap.css')}}">--}}
    <!-- Daterange picker -->
{{--    <link rel="stylesheet" href="{{asset('back-end/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">--}}
    <!-- bootstrap wysihtml5 - text editor -->
{{--    <link rel="stylesheet" href="{{asset('back-end/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">--}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{route('adminindex')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Student</b>Collab</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->

        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            @php
            $d=\App\StudentQuestion::where('permit','no')->get();
            @endphp

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <i class="fa fa-stack-exchange"></i>
                            @if(sizeof($d)>=1)
                            <span class="label label-danger"> {{sizeof($d)}} </span>

                        </a>
                        <ul class="dropdown-menu">
                            @foreach($d as $data)
                                <li>
                                    <ul class="menu">
                                        <li><!-- Task item -->
                                            <a href="#">
                                                <h3>{{$data->user->name}}</h3>
                                                <p>Posted a new question</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endforeach
                            <li class="footer">
                                <a href="{{route('newquestions')}}">View all questions</a>
                            </li>
                        </ul>

                        @endif
                    </li>


                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                            @php
                                $c=\App\User::where('approve','no')->get();
                            @endphp
                            @if(sizeof($c)>=1)
                                <span class="label label-danger"> {{sizeof($c)}} </span>

                        </a>
                        <ul class="dropdown-menu">
                            @foreach($c as $data)
                                <li>
                                    <ul class="menu">
                                        <li><!-- Task item -->
                                            <a href="#">
                                                <img src="{{asset('image/'.$data->image)}}" width="20px" alt=""><p>{{$data->name}} wants to join.</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endforeach
                            <li class="footer">
                                <a href="{{route('allusers')}}">View all</a>
                            </li>
                        </ul>

                        @endif
                    </li>

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('image/'.\Illuminate\Support\Facades\Auth::user()->image)}}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{asset('image/'.\Illuminate\Support\Facades\Auth::user()->image)}}" class="img-circle" alt="User Image">

                                <p>
                                    {{\Illuminate\Support\Facades\Auth::user()->name}}
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                {{--<div class="pull-left">--}}
                                    {{--<a href="#" class="btn btn-default btn-flat">Profile</a>--}}
                                {{--</div>--}}
                                <div class="pull-right">
                                    <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('image/'.\Illuminate\Support\Facades\Auth::user()->image)}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{\Illuminate\Support\Facades\Auth::user()->name}}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active treeview">
                    <a href="">
                        <i class="fa fa-dashboard"></i>

                        <span>Dashboard</span>
                        <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                </li>

                <li><a href="{{route('newquestions')}}"><i class="fa fa-book"></i> <span>New Questions</span></a></li>
                <li><a href="{{route('allusers')}}"><i class="fa fa-user"></i> <span>All users</span></a></li>
                {{--<li><a href="{{url('admin/addStudent')}}"><i class="fa fa-book"></i> <span>Add student</span></a></li>--}}
                {{--<li><a href="{{url('admin/viewStudent')}}"><i class="fa fa-book"></i> <span>View all student</span></a></li>--}}
                {{--<li><a href=""><i class="fa fa-book"></i> <span>Documentation</span></a></li>--}}
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('adminindex')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">All pending questions</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            {{--<!-- Small boxes (Stat box) -->--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-3 col-xs-6">--}}
                    {{--<!-- small box -->--}}
                    {{--<div class="small-box bg-aqua">--}}
                        {{--<div class="inner">--}}
                            {{--<h3>150</h3>--}}

                            {{--<p>New Orders</p>--}}
                        {{--</div>--}}
                        {{--<div class="icon">--}}
                            {{--<i class="ion ion-bag"></i>--}}
                        {{--</div>--}}
                        {{--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- ./col -->--}}
                {{--<div class="col-lg-3 col-xs-6">--}}
                    {{--<!-- small box -->--}}
                    {{--<div class="small-box bg-green">--}}
                        {{--<div class="inner">--}}
                            {{--<h3>53<sup style="font-size: 20px">%</sup></h3>--}}

                            {{--<p>Bounce Rate</p>--}}
                        {{--</div>--}}
                        {{--<div class="icon">--}}
                            {{--<i class="ion ion-stats-bars"></i>--}}
                        {{--</div>--}}
                        {{--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- ./col -->--}}
                {{--<div class="col-lg-3 col-xs-6">--}}
                    {{--<!-- small box -->--}}
                    {{--<div class="small-box bg-yellow">--}}
                        {{--<div class="inner">--}}
                            {{--<h3>44</h3>--}}

                            {{--<p>User Registrations</p>--}}
                        {{--</div>--}}
                        {{--<div class="icon">--}}
                            {{--<i class="ion ion-person-add"></i>--}}
                        {{--</div>--}}
                        {{--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- ./col -->--}}
                {{--<div class="col-lg-3 col-xs-6">--}}
                    {{--<!-- small box -->--}}
                    {{--<div class="small-box bg-red">--}}
                        {{--<div class="inner">--}}
                            {{--<h3>65</h3>--}}

                            {{--<p>Unique Visitors</p>--}}
                        {{--</div>--}}
                        {{--<div class="icon">--}}
                            {{--<i class="ion ion-pie-graph"></i>--}}
                        {{--</div>--}}
                        {{--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- ./col -->--}}
            {{--</div>--}}
            {{--<!-- /.row -->--}}

            @yield('content')

                </section>
                {{--<!-- right col -->--}}
            {{--</div>--}}
            {{--<!-- /.row (main row) -->--}}

        {{--</section>--}}
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        Student Collab 2018

    </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('back-end/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
{{--<script src="{{asset('back-end/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>--}}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
{{--<script>--}}
    {{--$.widget.bridge('uibutton', $.ui.button);--}}
{{--</script>--}}
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('back-end/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
{{--<script src="{{asset('back-end/bower_components/raphael/raphael.min.js')}}"></script>--}}
{{--<script src="{{asset('back-end/bower_components/morris.js/morris.min.js')}}"></script>--}}
<!-- Sparkline -->
{{--<script src="{{asset('back-end/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>--}}
<!-- jvectormap -->
{{--<script src="{{asset('back-end/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>--}}
{{--<script src="{{asset('back-end/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>--}}
<!-- jQuery Knob Chart -->
{{--<script src="{{asset('back-end/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>--}}
<!-- daterangepicker -->
{{--<script src="{{asset('back-end/bower_components/moment/min/moment.min.js')}}"></script>--}}
{{--<script src="{{asset('back-end/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>--}}
<!-- datepicker -->
{{--<script src="{{asset('back-end/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>--}}
<!-- Bootstrap WYSIHTML5 -->
{{--<script src="{{asset('back-end/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>--}}
<!-- Slimscroll -->
{{--<script src="{{asset('back-end/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>--}}
<!-- FastClick -->
{{--<script src="{{asset('back-end/bower_components/fastclick/lib/fastclick.js')}}"></script>--}}
<!-- AdminLTE App -->
{{--<script src="{{asset('back-end/dist/js/adminlte.min.js')}}"></script>--}}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{asset('back-end/dist/js/pages/dashboard.js')}}"></script>--}}
<!-- AdminLTE for demo purposes -->
{{--<script src="{{asset('back-end/dist/js/demo.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

@yield('script')
</body>
</html>
