@extends('admin.layout.master')

@section('content')

    <div class="row">

        <div class="col-lg-12">
            <div class="col-lg-8 col-lg-offset-3">
                <h4>Frequency of each topic</h4>
                <canvas id="totalTopic"></canvas>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            'use strict';
            $.ajax({
                url:"{{route('totalTopic')}}",
                method:"GET",
                success:function(data){
                    console.log(data);
                    var topicname =[];
                    var totalTppic= [];

                    for(var i in data){
                        topicname.push("Topic: "+data[i].tname);
                        totalTppic.push(data[i].totals);
                    }

                    var BARCHARTHOME = $('#totalTopic');
                    var barChartHome = new Chart(BARCHARTHOME, {
                        type: 'bar',
                        options:
                            {
                                scales:
                                    {
                                        xAxes: [{
                                            display:true,
                                            gridLines: {
                                                color: '#eee'
                                            }
                                        }],
                                        yAxes: [{
                                            display: true,
                                            ticks: {
                                                beginAtZero:true
                                            }
                                        }],
                                    },

                                legend: {
                                    display: true
                                }
                            },
                        data: {
                            labels: topicname,
                            datasets: [
                                {
                                    label: 'Total student',
                                    backgroundColor:['#4286f4','#011b44','#7a020e','#1e0149',
                                        '#033302','#827705','#f93100','#033d33','#0560ff',
                                        '#ff054f','#4f0219','#ff477e','#130344','#024430',
                                        '#66ffd0','#214700','#ff0000','#ffae00','#29560d',
                                        '#02ccff','#014dff','#2c066d',
                                        '#280033','#c83bef','#260130','#4f002b','#190811'],

                                    borderColor:
                                        '#fff',
                                    borderWidth: 1,
                                    data:totalTppic,
                                }
                            ]
                        }
                    });
                },

                error:function (data) {
//                console.log(data);
                }

            });
        });
    </script>
@endsection