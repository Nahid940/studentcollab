<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable =['user_id','comment_id','user_name','reply'];


    public function comment()
    {
        return $this->belongsTo('App\Comment','comments');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }




}
