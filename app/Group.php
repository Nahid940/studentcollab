<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $fillable=['group_name','user_id'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function groupmember()
    {
        return $this->belongsToMany('App\User');
    }


}

