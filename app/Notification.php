<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected  $fillable=['author_id','type','post_id','user_name','check'];

    public function user()
    {
        return $this->belongsTo('App\User','author_id');
    }

    public function blogPost()
    {
        return $this->belongsTo('App\BlogPost');
    }
}
