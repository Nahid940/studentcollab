<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionComment extends Model
{

    protected $fillable=['comment','discussion_id','user_name'];


}
