<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circle extends Model
{
    protected  $fillable=['user_id','user_id1','accept'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id1');
    }

    public function user1()
    {
        return $this->belongsTo('App\User','user_id');
    }
}

