<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blogcomment extends Model
{
    //

    protected $fillable=['user_id','blog_post_id','comment'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
