<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentQuestion extends Model
{
    //

    protected $fillable=['student_id','question'];

    public function comments()
    {
        return $this->hasMany('App\Comment','question_id')->orderBy('created_at','desc');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function topic()
    {
        return $this->belongsToMany('App\Topic','question_topics','question_id','topic_id');
    }

//    public function reply()
//    {
//        return $this->hasMany('App\Reply','replies','question_id');
//    }

    public function file()
    {
        return $this->hasMany('App\File','question_id');
    }

    public function mark()
    {
        return $this->hasMany('App\QuestionMark','question_id');
    }

    public function favourites()
    {
        return $this->hasOne('App\Favourites','question_id');
    }


}
