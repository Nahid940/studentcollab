<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionMark extends Model
{
    //

    protected $fillable=['question_id','user_id','point'];

    public function question()
    {
        return $this->belongsTo('App\StudentQuestion');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
