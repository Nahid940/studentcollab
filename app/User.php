<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','image','approve'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];




    public function roles()
    {
        return $this->belongsToMany('App\Roles','user_roles','user_id','role_id');
    }


    public function hasManyRole($roles)
    {
        if(is_array($roles))
        {
            foreach ($roles as $role)
            {
                if($this->hasRole($role))
                {
                    return true;
                }
            }
        }else{
            if($this->hasRole($roles))
            {
                return true;
            }
        }
        return false;
    }


    public function hasRole($role)
    {
        if($this->roles()->where('role_name',$role)->first())
        {
            return true;
        }
        return false;
    }


    public function questions()
    {
        return $this->hasMany('App\StudentQuestion'
        );
    }

    public function reply()
    {
        return $this->hasMany('App\Reply','replies','user_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment','comments','user_id');
    }

    public function circle()
    {
        return $this->hasMany('App\Circle','user_id');
    }

    public function mark()
    {
        return $this->hasMany('App\QuestionMark');
    }

    public function group()
    {
        return $this->hasMany('App\Group');
    }

    public function member()
    {
        return $this->belongsToMany('App\Group');
    }

    public function expertise()
    {
        return $this->belongsToMany('App\Topic','expertise')->withPivot('topic_id');
    }

    public function blogcomment()
    {
        return $this->hasMany('App\BlogComment');
    }

    public function isOnline()
    {
        return Cache::has('online'.$this->id);
    }






}
