<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //
    protected  $fillable=['question_id','user_id','filename'];
    public function question()
    {
        return $this->belongsTo('App\StudentQuestion');
    }
}
