<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    //
    protected  $fillable=['user_id','blog','image'];

    public function comment()
    {
        return $this->hasMany('App\BlogComment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
