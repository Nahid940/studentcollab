<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{

    protected $fillable=['group_id','discussion'];

    public function file()
    {
        return $this->hasMany('App\DiscussionFile');
    }

    public function comment()
    {
        return $this->hasMany('App\DiscussionComment');
    }
}

