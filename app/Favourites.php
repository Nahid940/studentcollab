<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourites extends Model
{
    //

    protected $fillable=['user_id','question_id'];

    public function question()
    {
        return $this->belongsTo('App\StudentQuestion','question_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
