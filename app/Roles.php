<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable=['user_id','role_id'];

    public function users()
    {
        return $this->belongsToMany('App\User','user_roles','role_id','user_id');
    }
}
