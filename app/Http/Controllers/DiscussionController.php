<?php

namespace App\Http\Controllers;

use App\Discussion;
use App\DiscussionComment;
use App\DiscussionFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiscussionController extends Controller
{
    public function addDiscussion(Request $request)
    {
        $d=Discussion::create(['group_id'=>$request->group_id,'discussion'=>$request->texts]);

        if($request->hasFile('image')) {

            $destinationPath = 'image/';
            for($i=0;$i<sizeof($request->file('image'));$i++)
            {
                $file = $request->file('image')[$i];

                $fileExt = $file->getClientOriginalExtension();

                $newFileName =  time().$i . '.' . $fileExt ;

                $file->storeAs($destinationPath, $newFileName);

                DiscussionFile::create(['discussion_id'=>$d->id,'file'=>$newFileName]);
            }
        }

    }

    public function getAllDiscussion($id)
    {
        $data=Discussion::with('file','comment')->where('group_id',$id)->get();
        return $data;
    }

    public function comment(Request $request)
    {
        DiscussionComment::create(['discussion_id'=>$request->id,'user_name'=>Auth::user()->name,'comment'=>$request->comment]);
    }
}
