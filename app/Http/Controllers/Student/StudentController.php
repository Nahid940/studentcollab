<?php

namespace App\Http\Controllers\Student;

use App\Comment;
use App\Favourites;
use App\Notification;
use App\QuestionMark;
use App\Reply;
use App\StudentQuestion;
use App\Topic;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index()
    {
        $topic=Topic::select('id','name')->get();
        $data = DB::table('question_topics')->select('topics.id','name',DB::raw('count(topic_id) as total'))
            ->leftJoin('topics','topics.id','=','question_topics.topic_id')
            ->groupBy('topic_id')->get();

        $posmark=QuestionMark::where('user_id',Auth::user()->id)->where('point',1)->count();
        $negmark=QuestionMark::where('user_id',Auth::user()->id)->where('point',0)->count();
        $totalQuestion=StudentQuestion::where('user_id',Auth::user()->id)->count();
        $favouriteLists=Favourites::where('user_id',Auth::user()->id)->count();



//        $d=DB::table('questions')->select('comment')
//            ->innerJoin('comments','questions.id','comments.question_id')
//            ->get();

        $totalquestion=DB::table('question_topics')->select(DB::raw('count(question_id) as total_asked'),'topic_id','name')
            ->leftJoin('topics','topics.id','question_topics.topic_id')
            ->groupBy('topic_id')->get();

            return view('student/index',compact('topic','data','posmark','negmark','totalQuestion','totalquestion','favouriteLists'));

    }

    public function questions(Request $request)
    {
        if($request->ajax()) {
            $data=StudentQuestion::with('user','comments','topic','favourites')->where('permit','yes')->orderBy('created_at','DESC')->get();
            $reply=Comment::with('reply')->get();
            $positivemark=QuestionMark::select('question_id',DB::raw('count(point) as totalpluspoint'))
                ->where('point',1)->groupBy('question_id')->get();
            $negativemark=QuestionMark::select('question_id',DB::raw('count(point) as totalnegativepoint'))
                ->where('point',0)->groupBy('question_id')->get();

            $datas=[$data,$reply,$positivemark,$negativemark];
            return json_encode($datas);


        }

    }


}
