<?php

namespace App\Http\Controllers\Student;

use App\Comment;
use App\Favourites;
use App\Notification;
use App\StudentQuestion;
use App\Topic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentQuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role')->except('studentpostquestion');
    }

    public function studentpostquestion(Request $request)
    {
        if($request!=null)
        {
            $id=DB::table('student_questions')
                ->insertGetId(['user_id'=>Auth::user()->id,
                    'question'=>$request->question,
                    'permit'=>'no',
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now()]);

            for($i=0;$i<sizeof($request->topic);$i++)
            {
                DB::table('question_topics')->insert(['question_id'=>$id,
                    'topic_id'=>$request->topic[$i],
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now()]);
            }
        }

        $data=StudentQuestion::with('user','topic')->where('id','=',$id)->first();
        return  json_encode($data);

    }

    public function postcomment(Request $request)
    {

        if (Auth::user()->hasRole('teacher')) {
            Comment::create(['user_id' => Auth::user()->id, 'user_name' => Auth::user()->name, 'question_id' => $request->question_id, 'comment' => $request->comment]);
            Notification::create(['author_id' => $request->author_id, 'type' => 'teacher', 'post_id' => $request->question_id, 'user_name' => Auth::user()->name]);
            return redirect()->back();
        }

        if (Auth::user()->id != $request->author_id){
            Notification::create(['author_id' => $request->author_id, 'type' => 'student', 'post_id' => $request->question_id, 'user_name' => Auth::user()->name]);
        }


        Comment::create(['user_id'=>Auth::user()->id,'user_name'=>Auth::user()->name,'question_id'=>$request->question_id,'comment'=>$request->comment]);
        return redirect()->back();

    }

    public function eachquestion($id,$id1)
    {
        $b=StudentQuestion::with('comments')->where('id',$id)->first();
//        $b=StudentQuestion::array('assignments' => function($query) {
//        $query->where('is_finished', '=', true)->orderBy('updated_at', 'desc');
//        })->where('id',$id)->first();


        Notification::where('id',$id1)->update(['check'=>'yes']);
        return view('student.eachQuestion',compact('b'));
    }

    public function ecachQuestion($id)
    {
        $question = Topic::with('question')->where('id', $id)->first();
        return view('student.questions',compact('question'));
    }

    public function favouritequestions()
    {
        $question=Favourites::with('question','user')->where('user_id',Auth::user()->id)->get();

        return view('student.favouritequestions',compact('question'));

    }
}
