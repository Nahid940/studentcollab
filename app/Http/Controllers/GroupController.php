<?php

namespace App\Http\Controllers;

use App\Circle;
use App\Group;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }


    public function index()
    {
        $data=Group::where('user_id',Auth::user()->id)->get();

        $datas=User::with('member')->where('id',Auth::user()->id)->first();

        $circle=Circle::with('user')->where('user_id',Auth::user()->id)->get();
        return view('student.group',compact('data','datas','circle'));
    }

    public function viewgroup($id)
    {
        $Groupdata=Group::with('groupmember')->where('id',$id)->first();


//        dd($Groupdata);

        return view('student.eachgroup',compact('Groupdata'));
    }

    public function createGroup(Request $request)
    {
        $d=Group::create(['group_name'=>$request->group_name,'user_id'=>Auth::user()->id]);

        for($i=0;$i<sizeof($request->id);$i++)
        {
            DB::table('group_user')->insert(['user_id'=>$request->id[$i],'group_id'=>$d->id]);
        }
    }

    public function deleteGroup($id)
    {
        Group::find($id)->delete();
        return redirect()->back()->with('deleted', 'Group deleted');
    }
}
