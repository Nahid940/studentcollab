<?php

namespace App\Http\Controllers\Teacher;

use App\StudentQuestion;
use App\Topic;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Question\Question;

class TeacherController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index()
    {

        $topic = User::with('expertise')->where('id', Auth::user()->id)->first();

        if (sizeof($topic ->expertise) >= 1){

            for ($i = 0; $i < sizeof($topic->expertise); $i++) {
                $questions = Topic::with('question')->where('id', $topic->expertise[$i]->id)->first();
            }

        return view('teacher.index', compact('questions'));
    }
    $questions=null;
        return view('teacher.index', compact('questions'));

    }

}
