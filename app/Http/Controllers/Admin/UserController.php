<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\SendConfirmaMailJob;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function allusers()
    {
        $allUsers=User::all();
        return view('admin.allusers',compact('allUsers'));
    }

    public function removeUser($id)
    {
        User::find($id)->delete();
        return redirect()->back()->with('delete_confirm','User deleted !');
    }

    public function approve($id)
    {
        $email=User::select('email')->where('id',$id)->first();
        User::where('id', $id)->update(['approve' => 'yes']);
        SendConfirmaMailJob::dispatch($email->email )->delay(Carbon::now()->addSeconds(10));
        return redirect()->back()->with('approve','User approved !');
    }
}
