<?php

namespace App\Http\Controllers\Admin;

use App\StudentQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionsController extends Controller
{
    public function newQuestions()
    {
        $data=StudentQuestion::with('user','topic')->where('permit','no')->get();
        return view('admin.newquestions',compact('data'));
    }
    public function approvequestion($id)
    {
        $permit = StudentQuestion::find($id);
        $permit->permit = 'yes';
        $permit->save();
        return redirect()->back()->with('approved','Question approved');
    }
    public function removequestion($id)
    {
        $permit = StudentQuestion::find($id);
        $permit->delete();
        return redirect()->back()->with('deleted','Question removed');
    }

}
