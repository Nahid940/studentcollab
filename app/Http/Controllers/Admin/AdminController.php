<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index()
    {

        return view('admin.index');
    }

    public function totalTopic()
    {
        $totalTopicUsed=DB::table('question_topics')
            ->select('topics.name as tname',DB::raw('COUNT(topic_id) as totals'))
            ->groupBy('topic_id')
            ->leftJoin('topics','topics.id','=','question_topics.topic_id')
            ->get();

        return $totalTopicUsed;
    }
}
