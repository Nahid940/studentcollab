<?php

namespace App\Http\Controllers;

use App\Blogcomment;
use App\BlogPost;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherBlogController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index()
    {
//
//        $blog=\App\BlogPost::with('user')->orderBy('created_at','desc')->get() ;
//        dd($blog);

        return view('teacher.blog');
    }

    public function addBlog(Request $request)
    {
        if($request->hasFile('image'))
        {
            $file=$request->image;
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $file->move('image/',$filename);
            $photo=$filename;
            $filename=($photo);
        }
        BlogPost::create(['user_id'=>Auth::user()->id,'blog'=>$request->texts,'image'=>$filename]);

        return redirect()->back()->with('blogPosted','Your blog has been posted !');
    }

    public function postcommentonblog(Request $request)
    {
        Blogcomment::create(['user_id'=>Auth::user()->id,'blog_post_id'=>$request->blog_id,'comment'=>$request->comment]);

        $id=BlogPost::select('user_id')->where('id',$request->blog_id)->first();
        if(Auth::user()->hasRole('student') && Auth::user()->id !=$request->author_id)
        {
            Notification::create(['author_id' => $id->user_id, 'type' => 'studentcomment', 'post_id' => $request->blog_id, 'user_name' => Auth::user()->name]);
        }

        return redirect()->back();
    }

    public function studentaccesstoblog()
    {
        return view('student.blogs');
    }

    public function eachblog($id,$id1)
    {

        $b=BlogPost::with('comment')->where('id',$id)->first();

        Notification::where('id',$id1)->update(['check'=>'yes']);
        return view('teacher.singleBlog',compact('b'));

    }
}
