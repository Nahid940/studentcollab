<?php

namespace App\Http\Controllers;


use App\Comment;
use App\Reply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function postreply(Request $request)
    {

        if($request->ajax()) {
            if ($request->commentid != '' && is_numeric($request->commentid) && $request->reply != '') {
                Reply::create(['user_id' => Auth::user()->id, 'comment_id' => $request->commentid, 'user_name' => Auth::user()->name, 'reply' => $request->reply]);
            }
            return $request;
        }
        Reply::create(['user_id' => Auth::user()->id, 'comment_id' => $request->commentid, 'user_name' => Auth::user()->name, 'reply' => $request->reply]);
        return redirect()->back();
    }
}
