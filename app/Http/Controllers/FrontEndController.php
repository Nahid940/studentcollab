<?php

namespace App\Http\Controllers;

use App\StudentQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontEndController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $data=StudentQuestion::with('user','topic','comments','mark')->orderBy('created_at','desc')->paginate(10);
        return view('index',compact('data'));
    }
}
