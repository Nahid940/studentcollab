<?php

namespace App\Http\Controllers;

use App\Favourites;
use App\QuestionMark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuestionPointController extends Controller
{
    //

    public function mark(Request $request)
    {
//
        $exist=DB::table('question_marks')
            ->where('question_id','=',$request->question_id)
            ->where('user_id','=',Auth::user()->id)
            ->first();

        if($exist) {

            if ($exist->point == 1 && $request->point==1) {
                DB::table('question_marks')
                    ->where('question_id','=',$exist->question_id)
                    ->where('user_id','=',$exist->user_id)
                    ->update(['point' => 1]);
            } else if($exist->point == 0 && $request->point==1){
            DB::table('question_marks' )
                ->where('question_id','=',$exist->question_id)
                ->where('user_id','=',$exist->user_id)
                ->update(['point' => 1]);
            }else if($exist->point == 1 && $request->point==0)
            {
                DB::table('question_marks' )
                    ->where('question_id','=',$exist->question_id)
                    ->where('user_id','=',$exist->user_id)
                    ->update(['point' => 0]);
            }
            else if($exist->point == 0 && $request->point==0)
            {
                DB::table('question_marks' )
                    ->where('question_id','=',$exist->question_id)
                    ->where('user_id','=',$exist->user_id)
                    ->update(['point' => 0]);
            }
        }

        else{
            QuestionMark::create(['question_id'=>$request->question_id,'user_id'=>Auth::user()->id,'point'=>$request->point]);
        }

    }

    public function favourite(Request $request)
    {
        $c=Favourites::where('user_id',Auth::user()->id)->where('question_id',$request->question_id)->count();

        if($c>=1)
        {
            Favourites::where('user_id',Auth::user()->id)->where('question_id',$request->question_id)->delete();
        }

        else{
            Favourites::create(['user_id'=>Auth::user()->id,'question_id'=>$request->question_id]);
        }


    }
}
