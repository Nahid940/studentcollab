<?php

namespace App\Http\Controllers;

use App\Circle;
use App\Roles;
use App\StudentQuestion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function users($id)
    {
         $d=null;
        $data=User::with('roles')->where('id',$id)->first();
        $questions=StudentQuestion::with('topic','comments')->where('user_id',$id)->get();

        if($id!=Auth::user()->id) {

            $d = Circle::with('user')->where('user_id', Auth::user()->id)->where('user_id1', $id)->first();

            if ($d == null) {
                $d = Circle::with('user1')->where('user_id1', Auth::user()->id)->where('user_id', $id)->first();
                return view('profile.profile', compact('data', 'd','questions'));
            }
            return view('profile.profile',compact('data','d','questions'));
        }

        else{
            $d="self";
            return view('profile.profile',compact('data','d','questions'));
    }

//        if(Auth::user()->id!=$id){
//            $exist=Circle::where('user_id',Auth::user()->id)
//                ->Where('user_id1',$id)
//                ->orWhere('user_id1',Auth::user()->id)
//                ->where('user_id',$id)
//                ->first();
////            $exist=Circle::where('user_id',Auth::user()->id)->orWhere('user_id',$id)->where('user_id1',Auth::user()->id)->orWhere('user_id1',$id)->first();
//                if($exist && $exist->accept=='yes')
//                {
//                    $questions=StudentQuestion::with('topic','comments')->where('user_id',$id)->get();
//                    $d=0;
//                    return view('profile.profile',compact('data','d','questions'));
//                }else if($exist && $exist->accept=='no'){
//                    $d=1;
//                    return view('profile.profile',compact('data','d'));
//                }else if ($exist && $exist->accept=='no' && $exist->user_id==Auth::user()->id){
//                    $d=4;
//                    return view('profile.profile',compact('data','d'));
//                }
//                else if($exist && $exist->accept=='no'){
//                    $d=2;
//                    return view('profile.profile',compact('data','d'));
//                }
//        }
//        else{
//            $d=3;
//            return view('profile.profile',compact('data','d'));
//        }

    }

    public function addnew($id)
    {
        $exist=Circle::where('user_id',Auth::user()->id)->where('user_id1',$id)->first();
        if($exist)
        {
            return redirect(route('users',['id'=>$id]));
        }else{
            Circle::create(['user_id'=>Auth::user()->id,'user_id1'=>$id,'accept'=>'no']);
            return redirect()->back()->with('requestSend','Request sent successfully !!');
        }
    }

    public function circle()
    {
        $data=Circle::with('user')->where('user_id',Auth::user()->id)->where('accept','yes')->get();
        return view('student.circle',compact('data'));
    }

    public function managecircle(Request $request)
    {
        Circle::where('id', $request->id)->delete();
    }

    public function addOrRemove(Request $request)
    {
        if($request->action=='accept') {
            Circle::where('id', $request->id)
                ->update(['accept' => 'yes']);
        }else if($request->action=='decline')
        {
            Circle::find($request->id)->delete();
        }

    }
}
