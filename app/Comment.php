<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable=['user_id','user_name','question_id','comment'];

    public function question()
    {
        return $this->belongsTo('App\StudentQuestion','student_questions');
    }

    public function reply()
    {
        return $this->hasMany('App\Reply','comment_id')->orderBy('created_at','desc');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
